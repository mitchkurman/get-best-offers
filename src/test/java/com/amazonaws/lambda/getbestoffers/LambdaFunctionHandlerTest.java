package com.amazonaws.lambda.getbestoffers;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.amazonaws.services.lambda.runtime.Context;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class LambdaFunctionHandlerTest {

    private static Object input;
    public static String TRANSACTION = "{'transaction_id':'15','year_month':'201307','agency_number':'1000','agency_name':'OKLAHOMA STATE UNIVERSITY','cardholder_last_name':'Meints','cardholder_first_initial':'K','description':'GENERAL PURCHASE','amount':'445','vendor':'OK DEPT OF VO-TECH ED','transaction_date':'7/30/13 0:00','posted_date':'7/31/13 0:00','merchant_category_code':'GOVERNMENT SERVICES--NOT ELSEWHERE CLASSIFIED'}";
   
    @BeforeClass
    public static void createInput() throws IOException {
        // TODO: set up your sample input object here.
        input = null;
    }

    private Context createContext() {
        TestContext ctx = new TestContext();

        // TODO: customize your context here if needed.
        ctx.setFunctionName("Your Function Name");

        return ctx;
    }
    
    @Test
    public void testLambdaFunctionHandler() {
        LambdaFunctionHandler handler = new LambdaFunctionHandler();
        Context ctx = createContext();
        LinkedHashMap<String,String> lhl = new LinkedHashMap<>();
        lhl.put("transaction", TRANSACTION);
        lhl.put("cardnumber", "372545000000012");
        String output = handler.handleRequest(lhl, ctx);
        System.out.println(output);

       //{'cardnumber':'372545000000006', 'customer_status':'platinum'}
        // TODO: validate output here if needed.
//        Assert.assertEquals("Hello from Lambda!", output);
    }
}
