package com.amazonaws.lambda.getbestoffers;

import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
//import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="rc_customer_offer")
public class CustomerOfferInfo {
//	{
//	    "uuid": "7d0c7006-27c2-4cb7-b493-5ae0e3c6037b",
//	    "last-name": "Mohiuddin",
//	    "first-initial": "I",
//	    "concatenated-name": "Mohiuddin I",
//	    "cardnumber": "372545000000014",
//	    "address" : "21 Henry Street Monroe Township, NJ 08831",
//	    "phone": "(200) 691-3694"
//	  }
		
		
		private String cardnumber;
		private String customerStatus;
		private List<CustomerOffer> historicalOffers;

		
	    
	    public CustomerOfferInfo() {
	    	
		}
	    
		@DynamoDBHashKey(attributeName="cardnumber")  
	    public String getCardnumber() { return cardnumber; }
	    public void setCardnumber(String cardnumber) {this.cardnumber = cardnumber; }
	    
	    @DynamoDBAttribute(attributeName="customer_status")  
	    public String getCustomerStatus() {return customerStatus; }
	    public void setCustomerStatus(String customerStatus) { this.customerStatus = customerStatus; }
	    
	    @DynamoDBAttribute(attributeName="extended_offers")  
	    public List<CustomerOffer> getHistoricalOffers() { return historicalOffers; }
	    public void setHistoricalOffers(List<CustomerOffer> historicalOffers) { this.historicalOffers = historicalOffers; }
	    
	    
//	    public String toString(){
//	    	return new StringBuffer("offerId :"+getOfferId()+ " s3Url: "+getS3Url() +" ...").toString();
//	    }

}