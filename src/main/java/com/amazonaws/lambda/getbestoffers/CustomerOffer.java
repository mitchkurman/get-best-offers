package com.amazonaws.lambda.getbestoffers;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class CustomerOffer {
	
	private String offerStatus;
	private Date offerExtendedDate;
	private String offerId;
	
	
	
	public CustomerOffer() {
	
	}
	
	@DynamoDBAttribute(attributeName="offer-status")  
	public String getOfferStatus() {
		return offerStatus;
	}
	public void setOfferStatus(String offerStatus) {
		this.offerStatus = offerStatus;
	}
	
	@DynamoDBAttribute(attributeName="offer-date")  
	public Date getOfferExtendedDate() {
		return offerExtendedDate;
	}
	public void setOfferExtendedDate(Date offerExtendedDate) {
		this.offerExtendedDate = offerExtendedDate;
	}
	
	@DynamoDBAttribute(attributeName="offer-id")  
	public String getOfferId() {
		return offerId;
	}
	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}
	
	
	

}
