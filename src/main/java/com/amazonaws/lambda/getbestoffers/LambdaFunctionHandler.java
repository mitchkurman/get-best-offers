package com.amazonaws.lambda.getbestoffers;
import java.io.StringReader;
import java.sql.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.PaginatedScanList;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.AmazonClientException;
import com.amazonaws.services.machinelearning.AmazonMachineLearningClient;
import com.amazonaws.services.machinelearning.model.PredictRequest;
import com.amazonaws.services.machinelearning.model.PredictResult;
import com.amazonaws.services.machinelearning.model.RealtimeEndpointInfo;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class LambdaFunctionHandler implements RequestHandler<LinkedHashMap<String,String>, String> {

    @Override
    public String handleRequest(LinkedHashMap<String,String> input, Context context) {
        context.getLogger().log("Input: " + input);
        context.getLogger().log("Class Type: "+input.getClass().getName()+"  ");
        
        // TODO: implement your handler
        OfferDetail demo = new OfferDetail();

        String transaction = input.get("transaction");
        String cardnumber = input.get("cardnumber");
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        
        //retrieve customer status from sending inputted transaction to prediction engine
        Gson transactionGson = new Gson();
        SampleTransaction st = transactionGson.fromJson(transaction, SampleTransaction.class);
       
        AmazonMachineLearningClient mlclient = new AmazonMachineLearningClient();
        PredictRequest request = new PredictRequest();
		request.setMLModelId("ml-nqrn1v5bfeh");
		request.setPredictEndpoint("https://realtime.machinelearning.us-east-1.amazonaws.com");
		request.addRecordEntry("Year Month", st.getYear_month())
		.addRecordEntry("Agency Number", st.getAgency_number())
		.addRecordEntry("Agency Name", st.getAgency_name()).addRecordEntry("Cardholder Last Name", st.getCardholder_last_name())
		.addRecordEntry("Cardholder First Initial", st.getCardholder_first_initial()).addRecordEntry("Description", st.getDescription())
		.addRecordEntry("Amount", st.getAmount()).addRecordEntry("Vendor", st.getVendor())
		.addRecordEntry("Transaction Date", st.getTransaction_date()).addRecordEntry("Posted Date", st.getPosted_date())
		.addRecordEntry("Merchant Category Code (MCC)", st.getMerchant_category_code());

		System.out.println("Sending prediction request for: "
				+ request.getRecord());

		// Send prediction request
		PredictResult result;
		try {
                        long start = System.currentTimeMillis();
                        result = mlclient.predict(request);
                        long end = System.currentTimeMillis();
                        System.out.println((end - start) + " ms");
		} catch (Exception e) {
			throw new AmazonClientException("Prediction failed", e);
		}

		// Display predicted value
		System.out.println("Predicted value:"
				+ result.getPrediction().getPredictedLabel());
	
        String custStatus = result.getPrediction().getPredictedLabel();
        
        
        
        //this logic gets the outstanding offers based on customer status//
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<String, AttributeValue>();
        expressionAttributeValues.put(":val1", new AttributeValue(custStatus));
        DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("status_code = :val1").withExpressionAttributeValues(expressionAttributeValues);
        List<OfferDetail> availableOffers = mapper.scan(OfferDetail.class, scanExpression);
        
        //this logic will get the customer offer status and history//
        Map<String, AttributeValue> eAV = new HashMap<String, AttributeValue>();
        eAV.put(":val1", new AttributeValue(cardnumber));
        DynamoDBScanExpression sE = new DynamoDBScanExpression().withFilterExpression("cardnumber = :val1").withExpressionAttributeValues(eAV);
        List<CustomerOfferInfo> existingCustomerOfferInformation = mapper.scan(CustomerOfferInfo.class, sE);
        //Finished setup - have all the data for moving forward//
        //---------------
        
        //Need logic here to match choose single best offer and to update offer history
        //each customer should only have 1 record returned at most
        boolean offerToMakeIsPending = false;
        OfferDetail offerToMake = null;
        if(!existingCustomerOfferInformation.isEmpty()){
        	context.getLogger().log("customer offers record exists in db");
	    	CustomerOfferInfo coi = (CustomerOfferInfo)existingCustomerOfferInformation.get(0);
	    	List<CustomerOffer> historyList = coi.getHistoricalOffers();
	    	CustomerOffer currentPendingOffer = null;
	    	List<CustomerOffer> noOffers = null;
	    	for (int i = 0; i<historyList.size();i++){
	    		//we want to pull any pending offers and redisplay; otherwise we have a list of offers to choose
	    		context.getLogger().log("get pending offer - should only be 1");
	    		CustomerOffer offerFromList = (CustomerOffer)historyList.get(i);
	    		if(offerFromList.getOfferStatus().equals("pend")){
	    			//goal is to ensure we get an answer for the offer before stopping//
	    			currentPendingOffer = offerFromList;
	    		}else{
	    			//don't want to offer one that is already declined or accepted//
	    			context.getLogger().log("all previous offered that aren't pending are either accept or decline - create a list of those");
	    			if(noOffers == null){
	    				noOffers = new ArrayList<CustomerOffer>();
	    			}
	    			noOffers.add(offerFromList);
    			}
	    	}
	    	
    		//now i have the current offer that i want to make, which i would use to go to offers table
    		//also i have offers i don't want to make since they've already been declined
    		//now i need to either get the offer detail from availableOffers to return the current offer details...
    		//or i need to pare down the list of results from offers to remove already declined offers
    		//if there are more than one left in the list, will randomize the return
    		//once the offer is selected to return, will update the customer offer history with a pending status
    		
    		if(currentPendingOffer != null){
    			//we need to get the offer details from the other table return
    			context.getLogger().log("there is a currentely pending offer, need to get the details to return");
    			String currentOfferId = currentPendingOffer.getOfferId();
    			for(int j=0; j<availableOffers.size();j++){
    				OfferDetail od = (OfferDetail)availableOffers.get(j);
    				if(od.getOfferId().equals(currentOfferId)){
    					offerToMake = od;
    					context.getLogger().log("found the offer details of the current pending offer, want to return this one");
    					//nothing more to do since offer is already marked as pending, just return
    					//need to ensure that this is tracked as pending
    					offerToMakeIsPending = true;
    				}
    			}
    		}else{
    			//we're going to go through scan result looking for 
    			context.getLogger().log("there is no pending offer, there may be previously actioned offers");
				if(noOffers == null || noOffers.isEmpty()){
					context.getLogger().log("the part where the offer is defaulted because no pending or previously actioned offers");
		    		offerToMake = (OfferDetail)availableOffers.get(0);
		    	}else{
		    		//this situation is there is no pending offer and there are offers to eliminate from the list
		    		//in theory we could end up with zero offers available to make
		    		context.getLogger().log("the part where offers are removed from the potential offers list because they have already been accepted or declined");
		    		List<OfferDetail> lco = new ArrayList<>();
		    		
		    		for(OfferDetail detail : availableOffers){
		    			boolean offerIsFound = false;
		    			for( CustomerOffer offer : noOffers){
		    				if((detail.getOfferId().equals(offer.getOfferId()))){
		    					offerIsFound = true;
		    					break;
		    				}
		    			}
		    			if(!offerIsFound){
		    				lco.add(detail);
		    			}
		    		}
		    		availableOffers = lco;
		    	}
	    		context.getLogger().log("now to handle the rest of the potential offers to make");
	    		//now we have a clean list of scan results, just get the 0th record to offer
	    		if(!availableOffers.isEmpty()){
	    			context.getLogger().log("if after processing, offers list still has offers, we'll take the first available");
	    			offerToMake = (OfferDetail)availableOffers.get(0);
	    			context.getLogger().log("no offer");
	    		}else{
	    			context.getLogger().log("no offers to make");
	    		}
	    		
    		}
        }
				
		//if there's an offer, it is either new or a pre-existing pend
		//for new, we write to db - either create a brand new customer record or append to an 
		//existing.  if pre-existing pend, no write to db.
					
		CustomerOfferInfo newCUI = null;
		if(offerToMake != null){
			context.getLogger().log("for the offer we want to make, need the offer details");
			if(!existingCustomerOfferInformation.isEmpty()){
				//this situation updates existing history record
				context.getLogger().log("determined existing history record to update "+offerToMake.getOfferId());
	        	newCUI = (CustomerOfferInfo)existingCustomerOfferInformation.get(0);	
			}
        }else{
        	context.getLogger().log("if after processing, offers list still has offers, we'll take the first available");
			if(!availableOffers.isEmpty()){
				offerToMake = (OfferDetail)availableOffers.get(0);
			}else{
				offerToMake = new OfferDetail();
				offerToMake.setS3Url("https://s3.amazonaws.com/rc-offers/no-offer.png");
				offerToMake.setOfferId("99999");
			}
			//context.getLogger().log("no offer");
        }
		context.getLogger().log("determined offer to make "+offerToMake.getOfferId());
		
		if(!offerToMakeIsPending && !offerToMake.getOfferId().equals("99999")){
			writeOfferToDb(cardnumber, custStatus, offerToMake, newCUI);
        }

    	context.getLogger().log("writing offer detail to json to return");
        Gson gson = new GsonBuilder().create();
        return gson.toJson(offerToMake, OfferDetail.class);
	}

    
    public void writeOfferToDb(String cardnumber, String status, OfferDetail makeOffer, CustomerOfferInfo cOI){
    	//this should take existing customer offer info and the selected offer detail and create a new history entry
    	//and update the customer offer info
    	
    	
    	CustomerOfferInfo newHistoryRecord = new CustomerOfferInfo();
        newHistoryRecord.setCardnumber(cardnumber);
        newHistoryRecord.setCustomerStatus(status);
        CustomerOffer newOffer = new CustomerOffer();
        newOffer.setOfferId(makeOffer.getOfferId());
        newOffer.setOfferExtendedDate(new GregorianCalendar().getTime());
        newOffer.setOfferStatus("pend");
        if(cOI == null){
	    	List<CustomerOffer> lco = new ArrayList<>();
	    	lco.add(0, newOffer);
	    	newHistoryRecord.setHistoricalOffers(lco); 
    	}else{
	        List<CustomerOffer> cod = cOI.getHistoricalOffers();
	    	cod.add(newOffer);
	    	newHistoryRecord.setHistoricalOffers(cod);
    	}
       //this logic should write to db
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
        DynamoDBMapper mapper = new DynamoDBMapper(client);
//        Map<String, AttributeValue> eAV = new HashMap<String, AttributeValue>();
//        eAV.put(":val1", new AttributeValue(cardnumber));
//        DynamoDBSaveExpression sE = new DynamoDBSaveExpression().getExpected(("cardnumber = :val1")).withExpressionAttributeValues(expressionAttributeValues);
        mapper.save(newHistoryRecord);
    	
    }


}