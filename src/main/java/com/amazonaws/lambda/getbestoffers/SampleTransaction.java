package com.amazonaws.lambda.getbestoffers;

public class SampleTransaction {
	private String transaction_id;
	private String year_month;
	private String agency_number;
	private String agency_name;
	private String cardholder_last_name;
	private String cardholder_first_initial;
	private String description;
	private String amount;
	private String vendor;
	private String transaction_date;
	private String posted_date;
	private String merchant_category_code;
	
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getYear_month() {
		return year_month;
	}
	public void setYear_month(String year_month) {
		this.year_month = year_month;
	}
	public String getAgency_number() {
		return agency_number;
	}
	public void setAgency_number(String agency_number) {
		this.agency_number = agency_number;
	}
	public String getAgency_name() {
		return agency_name;
	}
	public void setAgency_name(String agency_name) {
		this.agency_name = agency_name;
	}
	public String getCardholder_last_name() {
		return cardholder_last_name;
	}
	public void setCardholder_last_name(String cardholder_last_name) {
		this.cardholder_last_name = cardholder_last_name;
	}
	public String getCardholder_first_initial() {
		return cardholder_first_initial;
	}
	public void setCardholder_first_initial(String cardholder_first_initial) {
		this.cardholder_first_initial = cardholder_first_initial;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getTransaction_date() {
		return transaction_date;
	}
	public void setTransaction_date(String transaction_date) {
		this.transaction_date = transaction_date;
	}
	public String getPosted_date() {
		return posted_date;
	}
	public void setPosted_date(String posted_date) {
		this.posted_date = posted_date;
	}
	public String getMerchant_category_code() {
		return merchant_category_code;
	}
	public void setMerchant_category_code(String merchant_category_code) {
		this.merchant_category_code = merchant_category_code;
	}

}
