package com.amazonaws.lambda.getbestoffers;


//import java.util.Set;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
//import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;


@DynamoDBTable(tableName="rc_offers")
public class OfferDetail {
//	{
//	    "uuid": "7d0c7006-27c2-4cb7-b493-5ae0e3c6037b",
//	    "last-name": "Mohiuddin",
//	    "first-initial": "I",
//	    "concatenated-name": "Mohiuddin I",
//	    "cardnumber": "372545000000014",
//	    "address" : "21 Henry Street Monroe Township, NJ 08831",
//	    "phone": "(200) 691-3694"
//	  }
		public OfferDetail(){
			
		}
	
	    private String offerId;
	    private String s3Url;
	    private String offerName;
	    private String offerText;
	    private String commissionAmount;
	    private String customerStatusForOffer;

	    
	    @DynamoDBHashKey(attributeName="rc_offersId")  
	    public String getOfferId() { return offerId; }
	    public void setOfferId(String offerId) {this.offerId = offerId; }
	    
	    @DynamoDBAttribute(attributeName="s3_url")  
	    public String getS3Url() {return s3Url; }
	    public void setS3Url(String s3Url) { this.s3Url = s3Url; }
	    
	    @DynamoDBAttribute(attributeName="offer_name")  
	    public String getOfferName() { return offerName; }
	    public void setOfferName(String offerName) { this.offerName = offerName; }
	    
	    @DynamoDBAttribute(attributeName="offer_text")  
	    public String getOfferText() { return offerText; }
	    public void setOfferText(String offerText) { this.offerText = offerText; }
	    
	    @DynamoDBAttribute(attributeName="commission_amount")
	    public String getCommissionAmount() { return commissionAmount; }
	    public void setCommissionAmount(String commissionAmount) { this.commissionAmount = commissionAmount; }
	    
	    @DynamoDBAttribute(attributeName="status_code")
	    public String getCustomerStatusForOffer() { return customerStatusForOffer; }
	    public void setCustomerStatusForOffer(String customerStatusForOffer) { this.customerStatusForOffer = customerStatusForOffer; }
//	    
//	    @DynamoDBIgnore
//	    public String getSomeProp() { return someProp; }
//	    public void setSomeProp(String someProp) { this.someProp = someProp; }
	    
	    public String toString(){
	    	return new StringBuffer("offerId :"+getOfferId()+ " s3Url: "+getS3Url() +" ...").toString();
	    }

}
